package Utils;

import dao.SqlConnection;
import objects_models.Message;
import objects_models.Room;
import objects_models.User;

import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SqlDbUtils {
    private Connection connection = getDBConnection();

    public Connection getDBConnection() {
        return SqlConnection.getDBConnection();
    }

    private List<User> users = new ArrayList<>();

    static {
        Locale.setDefault(Locale.ENGLISH);
    }

    public boolean sendMessage(User user, Room room, String text, String dataTime) {
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO messages" +
                            "(text, userId, roomid)" +
                            "VALUES ( ?, ?, ?, ?) ");

            statement.setString(1, text);
            statement.setInt(2, user.getId());
            statement.setInt(3, room.getId());
            statement.setString(4, dataTime);
            statement.execute();

            System.out.println("Создана запись: " + text + " " + dataTime);

            statement.clearParameters();
            statement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean registrationUser(User newUser) {

        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO users" +
                            "(firstName, surName, login, password, photoUrl, rooms_id, status_online)" +
                            "VALUES ( ?, ?, ?, ?, ?, ?,?) "

            );

            statement.setString(1, newUser.getFirstName());
            statement.setString(2, newUser.getSurName());
            statement.setString(3, newUser.getLogin());
            statement.setString(4, newUser.getPassword());
            statement.setString(5, newUser.getPhotoUrl());
            statement.setString(6, "0");
            statement.setBoolean(7, false);
            statement.execute();

            System.out.println("Создана запись: " + newUser.getLogin());

            statement.clearParameters();
            statement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateChatRoom(Room room, User newUser) {

        try {
            PreparedStatement update = connection.
                    prepareStatement(
                            "UPDATE chat_rooms SET id = ?, name = ?, users_id = ?");

            update.setInt(1, room.getId());
            update.setString(2, room.getName());

            List<Integer> resultId = selectRoom(room).getUsersId();
            resultId.add(newUser.getId());
            StringBuilder stringOfId = new StringBuilder();

            for (int i = 0; i < resultId.size(); i++) {
                stringOfId.append(resultId.get(i) + ",");
            }
            String readyIds = String.valueOf(stringOfId);

            update.setString(3, readyIds);

            update.execute();
            System.out.println("Обновленная запись: " + room.getName());
            update.close();

//            System.out.println("Обновление прошло успешно!");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public User getUserByLoginAndPassword(User user) {

        String login = "'" + user.getLogin() + "'";
        String pass = "'" + user.getPassword() + "'";
        User userFromDB = null;
        try {
            PreparedStatement preparedStatement =
                    connection.prepareStatement(
                            "SELECT * FROM users WHERE user_login =" + "'" + user.getLogin() + "'" + " AND password =" + "'" + user.getPassword() + "'"

                            //"SELECT count(*) FROM users WHERE user_login = " + login + " and password =" + pass
                    );
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet == null) {
                return null;
            }

            ArrayList<Integer> roomsId = new ArrayList<>();

            while (resultSet.next()) {

                String roomsIDs = resultSet.getString(7);

                for (String tempId : roomsIDs.split(",")) {
                    System.out.println(tempId);
                    roomsId.add(Integer.valueOf(tempId));
                }

                userFromDB = new User(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        roomsId,
                        resultSet.getString(6),
                        resultSet.getBoolean(8)
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userFromDB;
    }

    public List<Message> getMessagesByRoomId(int roomId) {
        List<Message> allMessages = new ArrayList<>();

        try {
            PreparedStatement preparedStatement =
                    connection.prepareStatement(
                            "SELECT * FROM messages WHERE roomId = " + roomId
                    );
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                allMessages.add(new Message(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getInt(3),
                        resultSet.getInt(4),
                        resultSet.getString(5)
                ));
            }
            preparedStatement.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allMessages;
    }

    public Room selectRoom(Room room) {

        Room selectedRoom = new Room();
        ArrayList<Integer> listOfId = new ArrayList<>();
        try {
            PreparedStatement statement =
                    connection.prepareStatement(
                            "SELECT * FROM chat_rooms WHERE id =" + room.getId());

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            selectedRoom.setId(resultSet.getInt(1));
            selectedRoom.setName(resultSet.getString(2));


            //UTILS//

            String[] arrayOfId = resultSet.getString(3).split(",");

            for (int i = 0; i < arrayOfId.length; i++) {
                listOfId.add(Integer.parseInt(arrayOfId[i]));
            }

            selectedRoom.setUsersId(listOfId);

            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return selectedRoom;
    }

    public boolean createNewChatRoom(String nameOfRoom, User adminUser) {
        users.clear();
        users.add(adminUser);
        try {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO chat_rooms" +
                            "(name, users_id, messages_id)" +
                            "VALUES ( ?, ?, ?) "

            );

            statement.setString(1, nameOfRoom);

            String adminId = String.valueOf(adminUser.getId());
            statement.setString(2, adminId);
            statement.setString(3, "1");

            statement.execute();

            System.out.println("Создан чат с : " + adminUser.getLogin());

            statement.clearParameters();
            statement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void createNewTableChatRoom() {
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sqlString =
                    "CREATE TABLE table_message " +
                            "(id INTEGER not NULL, " +
                            " name VARCHAR(255), " +
                            " user_id INTEGER, " +
                            " PRIMARY KEY ( id ))";

            statement.executeUpdate(sqlString);
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
