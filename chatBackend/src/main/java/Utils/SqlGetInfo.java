package Utils;

import dao.SqlConnection;
import objects_models.Message;
import objects_models.Room;
import objects_models.User;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SqlGetInfo {

    private Connection connection = getDBConnection();

    public Connection getDBConnection() {
        return SqlConnection.getDBConnection();
    }

    SqlDbUtils sqlDbUtils = new SqlDbUtils();

    static {
        Locale.setDefault(Locale.ENGLISH);
    }

    public List<Message> getMessagesByRoom(Room room){
        return sqlDbUtils.getMessagesByRoomId(sqlDbUtils.selectRoom(room).getId());
    }

}
