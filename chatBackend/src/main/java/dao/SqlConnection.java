package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlConnection {
    private static final String USER = "root";
    private static final String PASS = "";

    private static final String URL_MYSQL =
            "jdbc:mysql://127.0.0.1:3306/chat_dev?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    public static Connection getDBConnection() {
        try {
            Class.forName ("com.mysql.jdbc.Driver");
            Connection dbConnection =
                    DriverManager.getConnection(URL_MYSQL, USER,PASS );

            return dbConnection;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
