package logicLayer;

import Utils.SqlDbUtils;
import objects_models.Room;
import objects_models.User;

public class UserSteps {

    private static SqlDbUtils sqlDbUtils;

    private static boolean verificationUser(User user) {
        if (sqlDbUtils.getUserByLoginAndPassword(user) == null) {
            System.out.println("Invalid user");
            return false;
        } else {
            User qwerty = sqlDbUtils.getUserByLoginAndPassword(user);
            System.out.println(qwerty.getLogin() + qwerty.getSurName() + " " + qwerty.getPassword());
            return true;
        }
    }

    public static boolean userRegistration(User newUser) {
        return sqlDbUtils.registrationUser(newUser);
    }

    public static boolean createChatRoom(String nameOfRoom, User userAdmin) {
        return sqlDbUtils.createNewChatRoom(nameOfRoom, userAdmin);
    }

    public static boolean addUserToChatRoom(Room room, User newUser){
        return sqlDbUtils.updateChatRoom(room, newUser);
    }

    public static boolean sendMessage(User user, Room room, String text, String time) {
        return sqlDbUtils.sendMessage(user, room, text, time);
    }
}
