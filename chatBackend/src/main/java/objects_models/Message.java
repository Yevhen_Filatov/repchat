package objects_models;

public class Message {
    private int id;
    private String text;
    private long userId;
    private int roomId;
    private String dataTime;

    public Message(int id, String text, long userId, int roomId, String dataTime) {
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.roomId = roomId;
        this.dataTime = dataTime;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
}
