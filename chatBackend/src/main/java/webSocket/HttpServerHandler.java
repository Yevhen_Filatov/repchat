package webSocket;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    WebSocketServerHandshaker handshaker;
    private final Gson gson = new Gson();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest httpRequest) throws Exception {

        switch (httpRequest.uri()) {
            case "/":
                HttpHeaders headers = httpRequest.headers();
                System.out.println("Connection : " + headers.get("Connection"));
                System.out.println("Upgrade : " + headers.get("Upgrade"));

                if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION)) &&
                        "WebSocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

                    //Adding new handler to the existing pipeline to handle WebSocket Messages
                    ctx.pipeline().replace(this, "websocketHandler", new WebSocketHandler());

                    System.out.println("WebSocketHandler added to the pipeline");
                    System.out.println("Opened Channel : " + ctx.channel());
                    System.out.println("Handshaking....");
                    //Do the Handshake to upgrade connection from HTTP to WebSocket protocol
                    handleHandshake(ctx, httpRequest);
                    System.out.println("Handshake is done");

                }
                break;
            case "/login":
                //   LoginMessage loginMessage = gson.fromJson(httpRequest.content().toString(StandardCharsets.UTF_8), LoginMessage.class);
                //   boolean isVerify = RepositoryService.verifyUser(loginMessage.getLogin(), loginMessage.getPassword());
                if (true) {
                    String jwtToken = "token";
                          /*
                              if (sqlDbUtils.getUserByLoginAndPassword(user) == null){
                                 System.out.println("Invalid user");
                                  }
                              else {
                                   User qwerty = sqlDbUtils.getUserByLoginAndPassword(user);
                                   System.out.println(qwerty.getLogin() + qwerty.getSurName() + " " + qwerty.getPassword() );
                                 }
                            */
                    ByteBuf responseBytes = ctx.alloc().buffer();

                    // responseBytes.writeBytes(jwtToken.getBytes());
                    responseBytes.writeBytes("True 100 proc".getBytes());

                    FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, responseBytes);
                    httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
                    httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH, httpResponse.content().readableBytes());
                    httpResponse.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

                    ctx.channel().writeAndFlush(httpResponse);
                }
                break;
        }
        System.out.println("Http Request Received");
    }

    /* Do the handshaking for WebSocket request */
    protected void handleHandshake(ChannelHandlerContext ctx, HttpRequest req) {
        WebSocketServerHandshakerFactory wsFactory =
                new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, true);
        handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);
        }
    }

    protected String getWebSocketURL(HttpRequest req) {
        System.out.println("Req URI : " + req.getUri());
        String url = "ws://" + req.headers().get("Host") + req.getUri();
        System.out.println("Constructed URL : " + url);
        return url;
    }
}
