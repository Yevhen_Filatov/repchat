package com.logger.bar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BarComponent {

	private static final Logger logger = LoggerFactory.getLogger(BarComponent.class);
	
	public void bar() {
		
		String name = "Here must be mistake";
		
		logger.info("Hello from logger.");
		
		
		logger.debug("I want to tell you{}.", name);
		
	}

}
