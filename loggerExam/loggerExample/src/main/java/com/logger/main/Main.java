package com.logger.main;

import com.logger.bar.BarComponent;
import com.logger.foo.FooComponent;

public class Main {

	public static void main(String args[]) {
		
		BarComponent barComponent = new BarComponent();
		barComponent.bar();
		
		FooComponent fooComponent = new FooComponent();
		fooComponent.foo();
		
	}
	
}
