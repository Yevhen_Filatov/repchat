package org.example.dto;

public class Params { /* слушаем от клиента */
    //registration command
    private String firstName;
    private String lastName;
    private String urlPhoto;
    private String userName;
    private String password;

    //create chat command
    private String chatName;

    //send message command
    private long timestamp; /* метка времени */
}
