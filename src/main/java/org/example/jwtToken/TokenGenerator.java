package org.example.jwtToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.RepositoryService;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class TokenGenerator {
    private static final Logger LOG = LoggerFactory.getLogger(TokenGenerator.class);
    private static final Map<String, Long> TOKEN_TIME_EXPIRED = new HashMap<>();/*время действия токена*/
    private static final String SECRET_KEY = "Супер секретный ключ";/*это наш ключ шифрования нашего логина и пароля*/

    public static String generateToken(String login, String password) {
        if (RepositoryService.verifyUser(login, password)) {
            Integer loginHashCode = login.hashCode();
            Integer passwordHashCode = password.hashCode();

            Mac hmacSha256 = null;
            try {
                hmacSha256 = Mac.getInstance("HmacSHA256"); /* викликали шифратор ы сазали шифруй по такому протоколу*/
            } catch (NoSuchAlgorithmException nsae) {
                LOG.error("No such algorithm");
            }

            SecretKeySpec secretKeySpec = new SecretKeySpec(SECRET_KEY.getBytes(StandardCharsets.UTF_8), "HmacSHA256"); /*байти підготовані до нашого кодеру */

            try {
                hmacSha256.init(secretKeySpec);/*передали секретний ключ, ти шифровальник, ти будеш шифрувати серетним ключем*/
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }
            // Build and return signature
            String token = Base64.getEncoder().encodeToString(hmacSha256.doFinal(
                    new byte[]{loginHashCode.byteValue(), passwordHashCode.byteValue()}));

            TOKEN_TIME_EXPIRED.put(token, Instant.now().plus(1, ChronoUnit.HOURS).toEpochMilli());
            return token;
        } else throw new IllegalArgumentException("We don't know this user"); /*There is no such user*/
    }

    public static boolean isTokenValid(String token) {/*отримали кешовані набори токенів в нашій мапі і будемо перевіряти був такий юзер в нашій мапі чи ні*/
        return TOKEN_TIME_EXPIRED.containsKey(token);
    }

    public static Long addTimeExpirationToken(long tokenTime, String token) {/*метод який продливає сесійний ключ*/
        return TOKEN_TIME_EXPIRED.put(token,
                Instant.ofEpochMilli(TOKEN_TIME_EXPIRED.get(token))
                        .plus(tokenTime, ChronoUnit.HOURS).toEpochMilli());
    }

    public static Long getTimeExpiration(String token) {
        return TOKEN_TIME_EXPIRED.get(token);
    }
}
