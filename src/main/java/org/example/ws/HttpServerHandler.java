package org.example.ws;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.RepositoryService;
import org.example.dto.LoginMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import org.example.jwtToken.TokenGenerator;

import java.nio.charset.StandardCharsets;

public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private static final Logger LOG = LoggerFactory.getLogger(HttpServerHandler.class);
    WebSocketServerHandshaker handshaker;

    @Override
    public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest httpRequest) {
        switch (httpRequest.uri()) {
            case "/org/example/ws": {
                HttpHeaders headers = httpRequest.headers();
                String jwtToken = headers.get("jwtToken");
                if (TokenGenerator.isTokenValid(jwtToken)) {
                    LOG.info("Connection : " + headers.get("Connection"));
                    LOG.info("Upgrade : " + headers.get("Upgrade"));
                    if ("Upgrade".equalsIgnoreCase(headers.get(HttpHeaderNames.CONNECTION)) &&
                            "WebSocket".equalsIgnoreCase(headers.get(HttpHeaderNames.UPGRADE))) {

                        //Adding new handler to the existing pipeline to handle WebSocket Messages
                        ctx.pipeline().replace(this, "websocketHandler", new WebSocketHandler());
                        LOG.info("WebSocketHandler added to the pipeline");
                        LOG.debug("Opened Channel : " + ctx.channel());
                        LOG.debug("Handshaking....");
                        //Do the Handshake to upgrade connection from HTTP to WebSocket protocol
                        handleHandshake(ctx, httpRequest);
                        LOG.info("Handshake is done");
                    }
                } else {
                    badResponseGenerate(ctx);
                }
                break;
            }

            case "/login":
                LoginMessage loginMessage = new Gson().fromJson(httpRequest.content().toString(StandardCharsets.UTF_8)
                        , LoginMessage.class);
                Boolean isVerify = RepositoryService.verifyUser(loginMessage.getLogin(), loginMessage.getPassword());

                String token = null;
                try {
                    token = TokenGenerator.generateToken(loginMessage.getLogin(), loginMessage.getPassword());
                } catch (IllegalArgumentException e) {
                    badResponseGenerate(ctx);
                }

                String jwtToken = "token";
                ByteBuf responseBytes = ctx.alloc().buffer();
                responseBytes.writeBytes(jwtToken.getBytes());
                FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, responseBytes);
                httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
                httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH, httpResponse.content().readableBytes());
                httpResponse.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);

                ctx.channel().writeAndFlush(httpResponse);
                break;
        }
    }

    private void badResponseGenerate(ChannelHandlerContext ctx) {
        FullHttpResponse httpResponse = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST);
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_TYPE, "application/json");
        httpResponse.headers().set(HttpHeaders.Names.CONTENT_LENGTH, httpResponse.content().readableBytes());
        httpResponse.headers().set(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
    }

    /* Do the handshaking for WebSocket request */
    protected void handleHandshake(ChannelHandlerContext ctx, HttpRequest req) {
        WebSocketServerHandshakerFactory wsFactory =
                new WebSocketServerHandshakerFactory(getWebSocketURL(req), null, true);
        handshaker = wsFactory.newHandshaker(req);
        if (handshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);
        }
    }

    protected String getWebSocketURL(HttpRequest req) {
        LOG.info("Req URI : " + req.getUri());
        String url = "org.example.ws://" + req.headers().get("Host") + req.getUri();
        LOG.info("Constructed URL : " + url);
        return url;
    }

    private void handleHandshake(ChannelHandlerContext ctx, FullHttpRequest httpRequest) {
    }
}
