package org.example.ws;

import com.google.gson.Gson;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.websocketx.*;
import org.example.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebSocketHandler extends ChannelInboundHandlerAdapter {/*обработка нового сообщения */
    private final Gson gson = new Gson();

    private static final Logger LOG = LoggerFactory.getLogger(WebSocketHandler.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {/*прийом всех сообщений*/

        if (msg instanceof WebSocketFrame) {
            LOG.info("This is a WebSocket frame");
            LOG.info("Client Channel : " + ctx.channel());
            if (msg instanceof BinaryWebSocketFrame) {
                LOG.info("BinaryWebSocketFrame Received : ");
                LOG.info(String.valueOf(((BinaryWebSocketFrame) msg).content()));
            } else if (msg instanceof TextWebSocketFrame) { /* сюда приходят все запросы от UI */
                LOG.info("TextWebSocketFrame Received : ");
                String message = ((TextWebSocketFrame) msg).text();
                gson.fromJson(message, Message.class);

                ctx.channel().writeAndFlush(/*запиши и закрой*/
                        new TextWebSocketFrame("Message recieved : " + (message))); /* логика, обработка запросов*/
                LOG.info(((TextWebSocketFrame) msg).text());
            } else if (msg instanceof PingWebSocketFrame) {
                LOG.info("PingWebSocketFrame Received : ");
                LOG.info(String.valueOf(((PingWebSocketFrame) msg).content()));
            } else if (msg instanceof PongWebSocketFrame) {
                LOG.info("PongWebSocketFrame Received : ");
                LOG.info(String.valueOf(((PongWebSocketFrame) msg).content()));
            } else if (msg instanceof CloseWebSocketFrame) { /* логика закрытия канала */
                LOG.info("CloseWebSocketFrame Received : ");
                LOG.debug("ReasonText :" + ((CloseWebSocketFrame) msg).reasonText());
                LOG.debug("StatusCode : " + ((CloseWebSocketFrame) msg).statusCode());
            } else {
                LOG.info("Unsupported WebSocketFrame");
            }
        }
    }
}
