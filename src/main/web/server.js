const express = require('express');

const app = express();
// var expressWs = require('express-org.example.ws')(app);
const http = require('http').createServer(app);
const io = require('socket.io')(http);

const cors = require('cors');
const uuidv4 = require('uuid').v4;

const rooms = {};
const chatLogs = {};

app.use(cors());

app.get('/room', function (req, res, next) {
  const room = {
    name: req.query.name,
    id: uuidv4(),
  };
  rooms[room.id] = room;
  chatLogs[room.id] = [];
  res.json(room);
});

app.get('/room/:roomId', function (req, res, next) {
  const { roomId } = req.params;
  const response = {
    ...rooms[roomId],
    chats: chatLogs[roomId],
  };
  res.json(response);
});

// app.org.example.ws('/', function (org.example.ws, req) {
// 	console.log("socket")
// 	org.example.ws.on('connection', function(socket){
// 		console.log("conect")
// 	})
// 	org.example.ws.on('essage', function (msg) {
// 		console.log(msg);
// 	});
// });

// app.listen(5000);

io.on('connection', function (socket) {
  socket.on('event://send-message', function (msg) {
    console.log('got', msg);

    const payload = JSON.parse(msg);
    if (chatLogs[payload.roomID]) {
      chatLogs[msg.roomID].push(payload.data);
    }

    socket.local.emit('event://get-message', msg);
  });
});

http.listen(5000, function () {
  console.log('listening on *:5000');
});
