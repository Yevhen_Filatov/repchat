import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import { BadgeAvatar } from '../index';

export const ChatList = (props) => {
  const { items, handleChangeRoom } = props;

  return (
    <List>
      {items.map((item, index) => {
        return (
          <ListItem key={`${index + 1}`} button onClick={() => handleChangeRoom(item._id)}>
            <ListItemIcon>
              <BadgeAvatar src={item.avatar || ''} alt={item.name} isOnline={item.isOnline} />
            </ListItemIcon>
            <ListItemText primary={item.name}>{item.name}</ListItemText>
          </ListItem>
        );
      })}
    </List>
  );
};
