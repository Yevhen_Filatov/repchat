import React, { useContext, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { WebSocketContext } from '../index';
import { Chat } from '../Chat/Chat';

export const ChatRoom = (props) => {
  const { rooms } = props;
  return (
    <div>
      <Chat rooms={rooms} />
    </div>
  );
};
