import React, { createContext } from 'react';
import { useDispatch } from 'react-redux';

import { WS_BASE } from '../../config.json';

export const WebSocketContext = createContext(null);

export const WebSocketProvider = ({ children }) => {
  let socket;
  let ws;

  // const dispatch = useDispatch();

  const sendMessage = (roomId, message) => {
    const payload = {
      roomId,
      data: message,
    };
    socket.emit('event://send-message', JSON.stringify(payload));
    // dispatch(sendMessageRequest(payload));
    socket.send(JSON.stringify(payload));
  };
  //
  if (!socket) {
    socket = new WebSocket(WS_BASE);
    //
    //   socket.on('event://get-message', (msg) => {
    //     const payload = JSON.parse(msg);
    //     dispatch(updateChatLog(payload));
    //   });
    socket.onmessage = function (event) {
      console.log(`[message] Данные получены с сервера: ${event.data}`);
    };

    socket.onclose = function (event) {
      if (event.wasClean) {
        console.log(`[close] Соединение закрыто чисто, код=${event.code} причина=${event.reason}`);
      } else {
        console.log(`[close] Соединение прервано`);
      }
    };

    socket.onerror = function (error) {
      console.log(`[error] ${error.message}`);
    };

    ws = {
      socket,
      sendMessage,
    };
  }

  return <WebSocketContext.Provider value={ws}>{children}</WebSocketContext.Provider>;
};
