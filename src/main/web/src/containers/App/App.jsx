import React from 'react';
import { Box } from '@material-ui/core';
import { Route, Switch } from 'react-router-dom';

// import { RouteWithCheckAuth } from '../../router/RouteWithCheckAuth';
import { Menu } from '../../components';
import { People } from '../People/People';
import { Conversations } from '../Conversations/Conversations';
import { About } from '../About/About';

export const App = () => {
  return (
    <Box>
      <Menu>
        <Switch>
          <Route path="/users" component={People} />
          <Route path="/community" component={Conversations} />
          <Route path="/about" component={About} />
        </Switch>
      </Menu>
    </Box>
  );
};
