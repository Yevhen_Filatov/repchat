import {
  APP_START_INIT,
  APP_FINISH_INIT,
  USER_GET_REQUEST,
  USER_GET_SUCCESS,
  USER_GET_ERROR,
  USERS_GET_SUCCESS,
  ROOMS_GET_SUCCESS,
} from './constants';

export const actionAppStartInit = () => ({ type: APP_START_INIT });
export const actionAppFinishInit = () => ({ type: APP_FINISH_INIT });

export const actionUserGetRequest = (payload) => ({ type: USER_GET_REQUEST, payload });
export const actionUserGetSuccess = (payload) => ({ type: USER_GET_SUCCESS, payload });
export const actionUserGetError = () => ({ type: USER_GET_ERROR });

export const actionUsersGetSuccess = (payload) => ({ type: USERS_GET_SUCCESS, payload });
export const actionRoomsGetSuccess = (payload) => ({ type: ROOMS_GET_SUCCESS, payload });
