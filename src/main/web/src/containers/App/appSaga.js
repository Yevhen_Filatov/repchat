/* Middleware, промежуточное ПО с помщью которого мы будем делать сетевые запросы */
import { put, takeLatest, all, call, select } from 'redux-saga/effects';
import axios from 'axios';

import { selectUserAuthData } from '../Auth/selectors';
import { APP_START_INIT } from './constants';
import {
  actionUserGetSuccess,
  actionUsersGetSuccess,
  actionRoomsGetSuccess,
  actionAppFinishInit,
} from './actions';

function* handleGetCurrentUser(id) {
  const requestUrl = `https://nestjs-chat-api.herokuapp.com/api/users/${id}`;

  try {
    return yield axios
      .get(requestUrl)
      .then((response) => response.data)
      .catch(() => {});
  } catch (e) {
    console.error(e);
    return {};
  }
}

function* handleGetUsers() {
  const requestUrl = 'https://nestjs-chat-api.herokuapp.com/api/users';

  try {
    return yield axios
      .get(requestUrl)
      .then((response) => response.data)
      .catch(() => []);
  } catch (e) {
    console.error(e);
    return [];
  }
}

function* handleGetRooms() {
  const requestUrl = 'https://nestjs-chat-api.herokuapp.com/api/rooms';

  try {
    return yield axios
      .get(requestUrl)
      .then((response) => response.data)
      .catch(() => []);
  } catch (e) {
    console.error(e);
    return [];
  }
}

function* handleAppStartInit() {
  try {
    const { user } = yield select(selectUserAuthData);
    const [currentUser, users, rooms] = yield all([
      call(handleGetCurrentUser, user._id),
      call(handleGetUsers),
      call(handleGetRooms),
    ]);

    yield put(actionUserGetSuccess({ currentUser }));
    yield put(actionUsersGetSuccess({ users }));
    yield put(actionRoomsGetSuccess({ rooms }));
    yield put(actionAppFinishInit());
  } catch (e) {
    console.error(e);
  }
}

export function* appSaga() {
  yield takeLatest(APP_START_INIT, handleAppStartInit);
}
