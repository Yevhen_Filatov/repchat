import { createSelector } from 'reselect';
import { initialState } from './appReducer';

export const selectAppDomain = (state) => state.app || initialState;
export const selectRouter = (state) => state.router;

export const selectIsReady = createSelector(selectAppDomain, (app) => app.isReady);
export const selectorCurrentUser = createSelector(selectAppDomain, (app) => app.currentUser);
