import { USER_AUTH_ERROR, USER_IS_AUTH_SET } from './constants';

export const initialState = {
  isAuth: false,
  isAuthError: false,
  userAuthData: null,
};

export const authReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case USER_IS_AUTH_SET: {
      const { isAuth, userAuthData } = payload;

      return { ...state, isAuth, userAuthData };
    }
    case USER_AUTH_ERROR: {
      const { isAuthError } = payload;

      return { ...state, isAuthError };
    }
    default:
      return state;
  }
};
