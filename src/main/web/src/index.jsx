import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware } from 'redux';
import { Provider, ReactReduxContext, useDispatch } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import {
  forceReducerReload,
  createInjectorsEnhancer,
  useInjectReducer,
  useInjectSaga,
} from 'redux-injectors';
import { ConnectedRouter, routerMiddleware } from 'connected-react-router';
import { Route, Switch } from 'react-router-dom';

import { storeCreator, history, getStoreReducer } from './utils';
import { AuthRoot } from './containers/Auth';
import { NotFoundPage } from './containers/NotFoundPage';
import { appReducer } from './containers/App/appReducer';
import { appSaga } from './containers/App/appSaga';
import { App } from './containers/App';
import { authSaga } from './containers/Auth/authSaga';
import { authReducer } from './containers/Auth/authReducer';
import { actionUserIsAuthSet } from './containers/Auth/actions';

const MOUNT_NODE = document.getElementById('root');

const ref = {
  store: null,
};

if (module.hot && module.hot.data && module.hot.data.store) {
  ref.store = module.hot.data.store;
  forceReducerReload(ref.store);
} else {
  const sagaMiddleware = createSagaMiddleware();

  const enhancers = [
    applyMiddleware(routerMiddleware(history), sagaMiddleware),
    createInjectorsEnhancer({ createReducer: getStoreReducer, runSaga: sagaMiddleware.run }),
  ];

  ref.store = storeCreator({
    reducer: getStoreReducer(),
    middleware: [],
    enhancers,
  });
}

const keyApp = 'app';
const keyAuth = 'auth';

const AppInit = () => {
  const dispatch = useDispatch();
  useInjectSaga({ key: keyAuth, saga: authSaga });
  useInjectReducer({ key: keyAuth, reducer: authReducer });
  useInjectReducer({ key: keyApp, reducer: appReducer });
  useInjectSaga({ key: keyApp, saga: appSaga });

  const userAuthData = localStorage.getItem('userAuthData');

  if (userAuthData) {
    dispatch(
      actionUserIsAuthSet({
        isAuth: true,
        userAuthData /* userAuthData: JSON.parse(userAuthData) */,
      }),
    );
  }

  return (
    <Switch>
      <Route path={['/sign-in', '/sign-up']} component={AuthRoot} />
      <Route path="/" component={App} />
      <Route component={NotFoundPage} />
    </Switch>
  );
};

const ConnectedApp = () => {
  return (
    <Provider store={ref.store} context={ReactReduxContext}>
      <ConnectedRouter history={history} context={ReactReduxContext}>
        <AppInit />
      </ConnectedRouter>
    </Provider>
  );
};

ReactDOM.render(<ConnectedApp />, MOUNT_NODE);
